package songJSON;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.ListIterator;

public class WriteJSONData {
    public static void main(String[] args) {
        ArrayList<Song> songList = new ArrayList<>();
        songList.add(new Song("Lose yourself", "Eminem", 4.00f));
        songList.add(new Song("Intro", "XX", 6.36f));
        songList.add(new Song("Slideaway", "Oasis", 3.34f));

        PrintWriter outputStream = null;

        try{
            outputStream = new PrintWriter(new FileWriter("jsonSongData.txt"));
            outputStream.write("{\"songs\" : [");

            ListIterator<Song> iter = songList.listIterator();
            while(iter.hasNext()){
                outputStream.write(iter.next().toJSONObject());

                if(iter.hasNext()){
                    outputStream.write(",");
                }
            }
            outputStream.write("]}");
        }catch(IOException io){
            io.printStackTrace();
        }finally{
            if(outputStream != null){
                outputStream.close();
            }
        }

    }
}
