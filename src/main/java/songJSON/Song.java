package songJSON;

public class Song {
    private String name;
    private String artist;
    private float runningTime;

    public Song(String name, String artist, float runningTime) {
        this.name = name;
        this.artist = artist;
        this.runningTime = runningTime;
    }

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public float getRunningTime() {
        return runningTime;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", artist='" + artist + '\'' +
                ", runningTime=" + runningTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Song)) return false;

        Song song = (Song) o;

        if (Float.compare(song.getRunningTime(), getRunningTime()) != 0) return false;
        if (getName() != null ? !getName().equals(song.getName()) : song.getName() != null) return false;
        return getArtist() != null ? getArtist().equals(song.getArtist()) : song.getArtist() == null;
    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getArtist() != null ? getArtist().hashCode() : 0);
        result = 31 * result + (getRunningTime() != +0.0f ? Float.floatToIntBits(getRunningTime()) : 0);
        return result;
    }

    /*************************************
     * Serialization
     */

    /*
    Builds a String as a JSON object representing the song.
    @return String containing the JSON data
     */
    public String toJSONObject(){
        return "{" + "\"name\":" + "\"" + this.name + "\"," + "\"artist\":" + "\"" + this.artist +"\"," + "\"," + "\"runningTime\":" + "\"" + this.runningTime +"}";
    }

    public String toCSV(){
        return this.name + "," + this.artist + "," + this.runningTime;
    }

    public String toHTMLTableData(){
        return "<td>" + this.name + "</td>" +
                "<td>" + this.artist + "</td>" +
                "<td>" + this.runningTime + "</td>";

    }

    public String toXML(){
        return "<name>" + this.name + "</name>" +
                "<artist>" + this.artist + "</artist>" +
                "<runningTime>" + this.runningTime + "</runningTime>";
    }
}
