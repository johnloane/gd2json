package dkit.gd2;

/* Retrieve JSON data from an API giving the location of the ISS
    Parse JSON using org.json. The format of the expected response from the API is
    {
  "message": "success",
  "timestamp": UNIX_TIME_STAMP,
  "iss_position": {
    "latitude": CURRENT_LATITUDE,
    "longitude": CURRENT_LONGITUDE
  }
}
 */

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class JSONParseSpaceStationLocation {
    public static void main(String[] args) {
        final String ISS_NOW_URI = "http://api.open-notify.org/iss-now.json";

        try{
            URL url = new URL(ISS_NOW_URI);
            InputStream in = url.openStream();
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            StringBuilder response = new StringBuilder();

            String inputStr;
            while((inputStr = streamReader.readLine()) != null){
                response.append(inputStr);
            }

            JSONObject responseJSON = new JSONObject(response.toString());
            JSONObject issPosition = responseJSON.getJSONObject("iss_position");
            String latitude = issPosition.getString("latitude");
            System.out.println(latitude);


        }catch(MalformedURLException e){
            System.out.println("Problem with the URL");
        }catch(IOException e){

        }
    }

}
