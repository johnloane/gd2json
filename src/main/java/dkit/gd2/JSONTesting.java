package dkit.gd2;

import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSONTesting {

    public static void main(String[] args) {
        createJSONObject();
        createJSONFromMap();
        createJSONObjectFromJSONString();
        User user = new User("Jason", 22, "Dundalk");
        serializeJavaObjectToJSON(user);
        createJSONArray();
        createJSONArrayFromJSONString();
        createJSONArrayFromJavaCollection();
        createJSONArrayFROMCSV();
        createCSVFromJSONArray();
        createJSONArrayOfJSONObjectFromCSV();
        createJSONArrayOfJSONObjectFromCSVWithoutHeader();
    }

    public static void createJSONObject(){
        JSONObject jo = new JSONObject();
        jo.put("name", "jason");
        jo.put("age", "22");
        jo.put("town", "Dundalk");

        System.out.println(jo);
    }

    public static void createJSONFromMap(){
        Map<String, String> map = new HashMap<>();
        map.put("name", "jason");
        map.put("age", "22");
        map.put("town", "Dundalk");

        JSONObject jo = new JSONObject(map);
        System.out.println(jo);

    }

    public static void createJSONObjectFromJSONString(){
        JSONObject jo = new JSONObject("{\"name\":\"jason\", \"age\":\"22\", \"town\":\"Dundalk\"}");
        System.out.println(jo);
    }

    public static void serializeJavaObjectToJSON(User user){
        JSONObject jo = new JSONObject(user);
        System.out.println(jo);
    }

    public static void createJSONArray(){
        JSONArray ja = new JSONArray();
        ja.put(Boolean.TRUE);
        ja.put("Jason");

        JSONObject jo = new JSONObject();
        jo.put("name", "jason");
        jo.put("age", "22");
        jo.put("town", "Dundalk");

        ja.put(jo);
        System.out.println(ja);
    }

    public static void createJSONArrayFromJSONString(){
        JSONArray ja = new JSONArray("[true, \"Jason\", 22]");
        System.out.println(ja);
    }

    public static void createJSONArrayFromJavaCollection(){
        List<String> list = new ArrayList<>();
        list.add("Cameron");
        list.add("Thomas");
        list.add("Adam");

        JSONArray ja = new JSONArray(list);
        System.out.println(ja);
    }

    public static void createJSONArrayFROMCSV(){
        JSONArray ja = CDL.rowToJSONArray(new JSONTokener("Nathan, Kristine, Neil"));
        System.out.println(ja);
    }

    public static void createCSVFromJSONArray(){
        JSONArray ja = new JSONArray(("[\"Paudric\", \"Nicholas\", \"Zehao\"]"));
        String csv = CDL.rowToString(ja);
        System.out.println(csv);
    }

    public static void createJSONArrayOfJSONObjectFromCSV(){
        String string = "name, age, town \n" +
                "jason, 22, Dundalk\n" +
                "cameron, 22, Rostrevor\n" +
                "james, 18, Ardee hey";

        JSONArray result = CDL.toJSONArray(string);
        System.out.println(result);
    }

    public static void createJSONArrayOfJSONObjectFromCSVWithoutHeader(){
        JSONArray ja = new JSONArray();
        ja.put("name");
        ja.put("age");
        ja.put("town");

        String string = "jason, 22, Dundalk\n" +
                "cameron, 22, Rostrevor\n" +
                "james, 18, Ardee hey";

        JSONArray result = CDL.toJSONArray(ja, string);
        System.out.println(result);

    }


}
